import React, { Component } from 'react'
import axios from 'axios'
import '../MediaGallery/style.css'
import Modal from 'react-bootstrap/Modal'
import { Button } from 'react-bootstrap';
import InfiniteScroll from 'react-infinite-scroll-component';

// eslint-disable-no-unused-vars
// eslint-disable-jsx-a11y/alt-text

export default class MediaGallery extends Component {

    // set initial state of elements
    constructor(props) {
        super(props);
        this.state = {
            searchText: window.localStorage.getItem('previousSearches').split(',')[window.localStorage.getItem('previousSearches').split(',').length - 1] || '',
            items: null,
            selectedImage: '',
            show: false,
            refreshing: false,
            previousSearches: window.localStorage.getItem('previousSearches').split(',') || []
        }

        this.handleChange = this.handleChange.bind(this);
        this.search = this.search.bind(this);
        this.imageURL = this.imageURL.bind(this);
    }

    fetchMoreData = (object) => {
        var that = object;
        setTimeout(() => {
            that.handleChange(true);
        }, 1000);
    };

    handleChange(addtoList) {
        if (this.state.searchText) {
            let match = false;
            for (let i = 0; i < this.state.previousSearches.length; i++) {
                if (this.state.previousSearches[i] === this.state.searchText) {
                    match = true;
                }
            }
            if (!match) {
                this.state.previousSearches.push(this.state.searchText);
                window.localStorage.setItem('previousSearches', this.state.previousSearches)
            }
        }
        var _this = this;
        _this.refreshing = true;
        this.serverRequest =
            axios
            .get(`https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=9e1527b6fb30d1609b400ee41ae5ac6b&text=${this.state.searchText}&format=rest&format=json&nojsoncallback=1&per_page=20`)
            .then(function(result) {
                if (addtoList) {
                    if (result && result.data && result.data.photos && _this.state.items && _this.state.items.length) {
                        _this.setState({
                            items: _this.state.items.concat(result.data.photos.photo),
                            selectedImage: _this.imageURL(result.data.photos.photo[0])
                        })
                        _this.refreshing = false;
                    } else if (result && result.data && result.data.photos) {
                        _this.setState({
                            items: result.data.photos.photo,
                            selectedImage: _this.imageURL(result.data.photos.photo[0])
                        })
                        _this.refreshing = false;
                    } else {
                        _this.setState({
                            items: _this.state.items,
                            // selectedImage: ''
                        })
                        _this.refreshing = true;
                    }
                } else {
                    if (result && result.data && result.data.photos) {
                        _this.setState({
                            items: result.data.photos.photo,
                            selectedImage: _this.imageURL(result.data.photos.photo[0])
                        })
                        _this.refreshing = false;
                    } else {
                        _this.setState({
                            items: [],
                            selectedImage: ''
                        })
                        _this.refreshing = true;
                    }
                }
            })
    }

    search(event) {
        this.setState({ searchText: event.target.value });
        setTimeout(() => {
            this.handleChange();
        }, 1000);
    }


    // assemble image URL
    imageURL(item) {
        return 'http://farm' + item.farm + '.staticflickr.com/' + item.server + '/' + item.id + '_' + item.secret + '.jpg'
    }


    // render the app
    render() {

            const handleClose = () => this.setState({ show: false });
            const handleShow = () => this.setState({ show: true });
            // handle image selection
            const selectImage = (selectedImage) => {
                this.setState({
                    selectedImage: selectedImage,
                });
                handleShow();
            }

            return ( < div > < div class = "suggestions" > {
                        this.state.previousSearches && this.state.previousSearches.length ?
                        this.state.previousSearches.map((search, index) =>
                            <
                            ol > < li > { search } < /li>  </ol >
                        ) : < div > No previous search queries < /div> } < /
                        div >

                        <
                        div className = "media-gallery" >
                        <
                        h1 className = "media-gallery__title" > Flickr API Component < /h1>  <
                        input type = "text"
                        class = "search-box"
                        value = { this.state.searchText }
                        onChange = { this.search }
                        />

                        <
                        InfiniteScroll dataLength = "20"
                        next = { this.fetchMoreData(this) }
                        hasMore = {!this.state.refreshing }
                        scrollThreshold = "1000px%"
                        loader = { < h4 > Loading... < /h4>}
                            endMessage = { <
                                p style = {
                                    { textAlign: 'center' }
                                } >
                                <
                                b > Yay!You have seen it all < /b> < /
                                p >
                            } >
                            <
                            div className = "media-gallery-thumbnails" > {
                                this.state.items && this.state.items.length ?
                                this.state.items.map((item, index) =>
                                    <
                                    div key = { index }
                                    onClick = { selectImage.bind(this, this.imageURL(item)) } >
                                    <
                                    img className = "media-gallery-thumbnails__img"
                                    alt = { this.imageURL(item) }
                                    src = { this.imageURL(item) }
                                    /> < /
                                    div > ) : (this.state.items && this.state.items.length === 0 ? < div > No photos matching search query.Please
                                        try with another search query... < /div> : <
                                        div > Loading... < /div>)
                                    } <
                                    /div>  < /
                                InfiniteScroll >

                                <
                                Modal show = { this.state.show }
                                onHide = { handleClose } >
                                <
                                Modal.Header closeButton >
                                <
                                Modal.Title > Selected Image < /Modal.Title> < /
                                Modal.Header > <
                                Modal.Body >
                                <
                                div className = "media-gallery-main" >
                                <
                                div >
                                <
                                img className = "media-gallery-main__img"
                                src = { this.state.selectedImage }
                                /> < /
                                div > <
                                /div> < /
                                Modal.Body > <
                                Modal.Footer >
                                <
                                Button variant = "secondary"
                                onClick = { handleClose } >
                                Close <
                                /Button> < /
                                Modal.Footer > <
                                /Modal>


                                <
                                /
                                div > < /div>
                            )
                        }
                    }